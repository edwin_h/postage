import org.newdawn.slick.Image;

public class MailPackage extends Entity{
    private static final int CHANCE_IS_WRONG = 48;

    private String destinationAddress;
    private String destinationCapitol;
    private String destinationState;
    private String destinationAbbreviation;
    private Image mailPackageImage;
    private boolean isMoving = false;

    public MailPackage(){
        Place alphaPlace = MailPackageInfo.pickRandomState();
        destinationAddress = MailPackageInfo.pickRandomAddress();
        if(Chance.percent(CHANCE_IS_WRONG)){
            Place bravoPlace;
            MailPackageInfo.pickRandomState();
            do{
                bravoPlace = MailPackageInfo.pickRandomState();
            }while(alphaPlace.equals(bravoPlace));

            destinationState = alphaPlace.getName();
            destinationCapitol = alphaPlace.getCapitol();
            destinationAbbreviation = alphaPlace.getAbbreviation();

            switch(Chance.integer(1,3)){
                case 1:
                    destinationState = bravoPlace.getName();
                    break;
                case 2:
                    destinationCapitol = bravoPlace.getCapitol();
                    break;
                case 3:
                    destinationAbbreviation = bravoPlace.getAbbreviation();
                    break;
            }

        }else{
            destinationCapitol = alphaPlace.getCapitol();
            destinationState = alphaPlace.getName();
            destinationAbbreviation = alphaPlace.getAbbreviation();
        }
    }

    public String getDestinationCapitol() {
        return destinationCapitol;
    }

    public String getDestinationState() {
        return destinationState;
    }

    public String getDestinationAbbreviation() {
        return destinationAbbreviation;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public Image getMailPackageImage() {
        return mailPackageImage;
    }

    public void setMailPackageImage(Image mailPackageImage) {
        this.mailPackageImage = mailPackageImage;
    }
}
