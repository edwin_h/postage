import java.util.ArrayList;
import java.util.Random;

public class Chance {
    private static Random r = new Random();
    public static int integer(int min, int max){
        return r.nextInt((max-min)+1)+min;
    }
    public static Object pick(ArrayList<Object> list){
        int n = integer(0, list.size() - 1);
        return list.get(n);
    }

    public static boolean percent(int perc){
        return integer(0, 100) < perc;
    }
}
