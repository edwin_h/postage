class Place {
    private String capitol, name, abbreviation;
    public Place(String st, String abbr, String cptl){
        this.capitol = cptl;
        this.name = st;
        this.abbreviation = abbr;
    }

    public String getCapitol() {
        return capitol;
    }

    public String getName() {
        return name;
    }

    public String getAbbreviation() {
        return abbreviation;
    }
}