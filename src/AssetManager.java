import org.newdawn.slick.Image;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class AssetManager {
    private final String ASSETS_ROOT = "data";

    private Map<String, Image> loadedAssetsMap;

    public AssetManager(){
        loadedAssetsMap = new HashMap<>();
    }
    public Image getAsset(String assetName){
        return loadedAssetsMap.get(assetName);
    }
    public void loadAssets(String assetsFileName){
        try{
            File assetsFile = new File(assetsFileName);
            parseAssetsFile(assetsFile);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public Image getAssetFromGroup(String groupName){
        ArrayList<Image> results = new ArrayList<>();
        for(String assetName : loadedAssetsMap.keySet()){
            if(assetName.startsWith(groupName)){
                results.add(loadedAssetsMap.get(assetName));
            }
        }
        int n = Chance.integer(0, results.size()-1);
        return results.get(n);
    }
    private void parseAssetsFile(File file){
        ArrayList<String> lines = new ArrayList<>();
        try {
            Scanner s = new Scanner(file);
            while(s.hasNextLine()){
                lines.add(s.nextLine());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        for(String line : lines){
            String[] parts = line.split(",");
            String name = parts[0];
            String filename = parts[1];
            String group = parts[2];

            String storedName;

            if(group.equals("null")){
                storedName = name;
            }else{
                storedName = group + "-" + name;
            }

            String imageLocation = ASSETS_ROOT+"/"+filename;

            try{
                Image image = new Image(imageLocation);
                image.setFilter(Image.FILTER_NEAREST);
                loadedAssetsMap.put(storedName, image);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        System.out.println("Loaded "+loadedAssetsMap.size()+" image assets");
    }
}
