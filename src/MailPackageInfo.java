import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class MailPackageInfo {
    private final String STATES_LIST = "data/statesList.txt";
    private final String STREETS_LIST = "data/streetsList.txt";

    public static ArrayList<Place> places;
    private static ArrayList<String> streets;
    private static ArrayList<String> streetAppends;
    public MailPackageInfo(){
        places = new ArrayList<>();
        streets = new ArrayList<>();

        loadStates(STATES_LIST);
        loadStreets(STREETS_LIST);

        streetAppends = new ArrayList<>();
        streetAppends.add("st");
        streetAppends.add("ave");
        streetAppends.add("ct");
        streetAppends.add("blvd");
        streetAppends.add("dr");
        streetAppends.add("rd");
        streetAppends.add("pl");
    }

    public static Place pickRandomState(){
        int n = Chance.integer(0, places.size()-1);
        return places.get(n);
    }

    public static String pickRandomAddress(){
        int address = Chance.integer(1,2000);
        String streetName = streets.get(Chance.integer(0, streets.size() - 1));
        String append = streetAppends.get(Chance.integer(0, streetAppends.size()-1));

        return (address + " " + streetName + " " + append + ".").toUpperCase();
    }

    private void loadStates(String statesFileLocation){
        try{
            File statesFile = new File(statesFileLocation);
            Scanner s = new Scanner(statesFile);
            while(s.hasNextLine()){
                String line = s.nextLine();
                String[] parts = line.split(",");

                Place place = new Place(parts[0], parts[1], parts[2]);
                places.add(place);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void loadStreets(String streetsFileLocation){
        try{
            File streetsFile = new File(streetsFileLocation);
            Scanner s = new Scanner(streetsFile);
            while(s.hasNextLine()){
                String line = s.nextLine();
                streets.add(line);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static String getCapitolOfState(String state){
        for(Place place : places){
            if(place.getName().equals(state)){
                return place.getCapitol();
            }
        }
        return null;
    }



}
