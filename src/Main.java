import org.newdawn.slick.*;
import org.newdawn.slick.font.effects.ColorEffect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Main extends BasicGame{
    private static final String TITLE = "Postage";
    private AppGameContainer appgc;

    private AssetManager assetManager;
    private ArrayList<MailPackage> mailPackages;
    private UnicodeFont font;

    private SpriteSheet stampsSprite;
    private Stamp currentStamp;
    private boolean stampIsDown;
    private Entity stampMark;

    private Entity nextButton;

    private int correct = 0;
    private int incorrect = 0;

    @Override
    public void init(GameContainer gc) throws SlickException {
        new MailPackageInfo();
        mailPackages = new ArrayList<>();
        assetManager = new AssetManager();
        assetManager.loadAssets("data/assetsList.txt");

        stampMark = null;
        currentStamp = Stamp.NONE;
        stampsSprite = new SpriteSheet(assetManager.getAsset("stamps"), 32, 32);
        stampIsDown = false;

        nextButton = new Entity();
        nextButton.setImage(assetManager.getAsset("nextbutton"));
        nextButton.setX(gc.getWidth()-nextButton.getImage().getWidth());
        nextButton.setY(gc.getHeight()/2 - (nextButton.getImage().getHeight()/2));

        for(int i = 0; i < 3; i++){
            addMailPackage();
        }

        try{
            font = new UnicodeFont("data/kenpixel_mini.ttf", 18, false, false);
            font.getEffects().add(new ColorEffect());
            font.addAsciiGlyphs();
            font.loadGlyphs();

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void update(GameContainer gc, int dt) throws SlickException {
        appgc.setTitle(TITLE + " @ " + appgc.getFPS() + " fps");
        font.loadGlyphs();

        if(currentStamp != Stamp.NONE){
            gc.setMouseGrabbed(true);
        }else{
            gc.setMouseGrabbed(false);
        }
    }


    @Override
    public void keyPressed(int key, char c) {
        super.keyPressed(key, c);
        switch(key){
            case Input.KEY_SPACE:
                stampIsDown = true;

                if(currentStamp != Stamp.NONE){
                    stampMark = new Entity();
                    stampMark.setX(appgc.getInput().getMouseX()-48);
                    stampMark.setY(appgc.getInput().getMouseY()+28);
                    stampMark.setScale(3f);
                    if(currentStamp == Stamp.APPROVE){
                        stampMark.setImage(assetManager.getAsset("ink-approve"));
                    }else if(currentStamp == Stamp.DENY){
                        stampMark.setImage(assetManager.getAsset("ink-deny"));
                    }
                }


                break;

        }
    }

    @Override
    public void keyReleased(int key, char c) {
        super.keyReleased(key, c);
        switch(key){
            case Input.KEY_SPACE:
                stampIsDown = false;
                break;
        }
    }



    @Override
    public void mousePressed(int button, int x, int y) {
        super.mousePressed(button, x, y);
        switch(button){
            case 0:
                // left mouse
                if(nextButton.isVisible() &&
                   x > nextButton.getX() &&
                   x < nextButton.getX()+nextButton.getImage().getWidth() &&
                   y > nextButton.getY() &&
                   y < nextButton.getY()+nextButton.getImage().getHeight()){
                    // Wants next mailpackage
                    checkCurrentMailPackage();
                    nextMailPackage();
                }else{
                    if(currentStamp == Stamp.APPROVE){
                        currentStamp = Stamp.NONE;
                    }else{
                        currentStamp = Stamp.APPROVE;
                    }
                }
                break;
            case 1:
                // right mouse
                if(currentStamp == Stamp.DENY){
                    currentStamp = Stamp.NONE;
                }else{
                    currentStamp = Stamp.DENY;
                }

                break;
        }

        System.out.println(currentStamp);
    }

    @Override
    public void render(GameContainer gc, Graphics g) throws SlickException {
        g.setFont(font);

        // Render the floor
        Image floor = assetManager.getAsset("cementfloor");
        floor.draw(0, 0);

        // Render the rollers
        Image rollers = assetManager.getAsset("rollers");
        float rollersScale = (float) ((double)gc.getWidth()/(double)rollers.getWidth());
        rollers.draw(0, gc.getHeight() - (rollers.getHeight()*rollersScale), rollersScale);

        // Render the mailpackages
        for(MailPackage mailPackage : mailPackages){
            int id = mailPackages.indexOf(mailPackage);

            Image image = mailPackage.getMailPackageImage();
            float scale = 3f;
            double imageWidth = (image.getWidth()*scale);
            double imageHeight = (image.getHeight()*scale);
            int x = (int) ((gc.getWidth()/2)-(imageWidth/2) - (id * (imageWidth + 32)));
            int y = (int) (gc.getHeight() - (image.getHeight()*scale) - 64);
            image.draw(x, y, scale);

            // Stick a tag to the mailpackage
            if(id > 0){
                Image tagImage = assetManager.getAsset("tag");
                tagImage.draw(x+16, y+16);
            }

        }

        // Render the tag for the current mailpackage
        Image tagImage = assetManager.getAsset("tag");
        float tagImageScale = 7f;
        double tagImageWidth = (tagImage.getWidth()*tagImageScale);
        double tagImageHeight = (tagImage.getHeight()*tagImageScale);
        int tagImageX = (int) ((gc.getWidth()/2)-(tagImageWidth/2));
        int tagImageY = (int) (gc.getHeight() - (tagImage.getHeight()*tagImageScale) - 192);
        tagImage.draw(tagImageX, tagImageY, tagImageScale);

        // Render the tag text
        if(mailPackages.size()>0){
            MailPackage currentPackage = mailPackages.get(0);
            int fontsize = font.getFont().getSize();
            Map<String,String> tagItems = new HashMap<>();
            tagItems.put("ADDR", currentPackage.getDestinationAddress());
            tagItems.put("STATE", currentPackage.getDestinationState());
            tagItems.put("CITY", currentPackage.getDestinationCapitol());

            Color textColor = new Color(127, 117, 105);
            int textPadding = 16;
            int textSpacing = 48;
            int textX = tagImageX + textPadding;
            int textY = tagImageY + textPadding;

            for(int i = 0; i < tagItems.size(); i++){
                String key = (String) tagItems.keySet().toArray()[i];
                String val = tagItems.get(key);
                g.setColor(textColor.brighter());
                g.drawString(key, textX, textY+(i*textSpacing));

                g.setColor(textColor);
                g.drawString(val, textX, textY+(i*textSpacing)+fontsize);
            }

            // Render the stampmark
            if(stampMark != null){
                Image markImage = stampMark.getImage();
                markImage.draw(stampMark.getX(), stampMark.getY(), stampMark.getScale());
            }

            // Render the stamp
            float stampScale = 5f;
            Image stampImage = null;
            int add = 0;
            if(stampIsDown){
                add = 1;
            }
            switch(currentStamp){

                case APPROVE:
                    stampImage = stampsSprite.getSprite(0+add, 0);
                    break;
                case DENY:
                    stampImage = stampsSprite.getSprite(0+add, 1);
                    break;
                case NONE:
                    break;
            }

            if(stampImage != null){
                int stampX = gc.getInput().getMouseX() - (int)((stampImage.getWidth()*stampScale)/2);
                int stampY = gc.getInput().getMouseY() - (int)((stampImage.getHeight()*stampScale)/2);
                stampImage.draw(stampX, stampY, stampScale);
            }
        }

        // Render the next button
        nextButton.getImage().draw(nextButton.getX(), nextButton.getY());

        // Render correct / incorrect
        g.setColor(new Color(220, 220, 220));
        g.drawString("Correct: "+correct, 32, 32);
        g.drawString("Incorrect: "+incorrect, 32, 64);
    }

    private void nextMailPackage(){
        mailPackages.remove(0);
        stampMark = null;
        addMailPackage();
    }
    private void addMailPackage(){
        MailPackage mp = new MailPackage();
        /*
        mp.setX(-256);
        mp.setY(536);
        */
        mp.setMailPackageImage(assetManager.getAssetFromGroup("boxes"));
        mailPackages.add(mp);
    }
    private void checkCurrentMailPackage() {
        String state = mailPackages.get(0).getDestinationState();
        String capitol = mailPackages.get(0).getDestinationCapitol();

        String correctCapitol = MailPackageInfo.getCapitolOfState(state);

        if(capitol.equals(correctCapitol)){
            correct++;
        }else{
            incorrect++;
        }
    }

    public Main(String title){
        super(title);
        try{
            appgc = new AppGameContainer(this, 800, 600, false);
            appgc.setVSync(true);
            appgc.setShowFPS(false);
            appgc.start();
        }catch(SlickException e){
            e.printStackTrace();
        }
    }
    public static void main(String[] args){
        new Main(TITLE);
    }
}
